<?php

/**
 * GET Empleados
 * POST New Empleado
 * POST(GET ID) Update Empleado
 * DELETE delete empleado
 */

use LDAP\Result;

require "classConectar.php";
$method = $_SERVER['REQUEST_METHOD'];
$campos = [
    ["nombre", "El nombre es requerido"],
    ["email", "El Email es requerido"],
    ["sexo", "El Sexo es requerido"],
    ["area_id", "El area es requerida"],
    ["boletin", "El boletin es requerido"],
    ["descripcion", "La descripcion es requerida"]
];
switch ($method) {
    case 'GET':
        // Se retornan todos los resultados de la tabla
        $objCon = new Conectar();
        $sqlEmple = "
                SELECT em.id,em.nombre, em.email,em.sexo,a.nombre as area, case em.boletin when 0 then 'No' when 1 then 'Si' end as boletin,em.boletin, a.id, em.descripcion
                    From empleado em
                    inner join areas a on em.area_id = a.id
            ";
        $empleados = $objCon->exe($sqlEmple);
        echo json_encode($empleados);
        break;

    case 'POST':
        $objCon = new Conectar();
        if (!isset($_GET["id"])) {
            $sql = "INSERT INTO `empleado` (";
            $atributo = "";
            $values = "";
            foreach ($campos as $campo) {
                // Se valida que la estructura del empleado este correcta.
                if (!isset($_POST[$campo[0]])) {
                    echo json_encode(["error", $campo[1]]);
                    die;
                }
                // Despues de validar arma el query con los datos.
                $atributo .= "`" . $campo[0] . "`,";
                $values .= "'" . $_POST[$campo[0]] . "',";
            }
            $sql .= substr($atributo, 0, -1) . ") values (" . substr($values, 0, -1) . ");";
            // Se registra el nuevo empleado.
            $result = $objCon->onlyID($sql);
            if ($result == false) {
                echo json_encode(["error", "No se registro..."]);
            } else {
                // se agregan los roles del empleado
                $sqlRol = "INSERT into empleado_rol values ";
                foreach ($_POST["roles"] as $rol) {
                    $sqlRol .= "(" . $result . "," . $rol . "),";
                }
                $sqlRol =  substr($sqlRol, 0, -1) . ";";
                $resulRol = $objCon->only($sqlRol);
                if ($resulRol == false) {
                    echo json_encode(["error", "No se registro..."]);
                } else {
                    echo json_encode(["succes", "Se registro Correctamente"]);
                }
            }
        } else {
            // Se arma el query para la actualizacion
            $sql = "UPDATE `empleado` SET ";
            foreach ($campos as $campo) {
                // Se valida que la estructura del empleado para saber que campo actualizar.
                if (isset($_POST[$campo[0]])) {
                    // Despues de validar arma el query con los datos.
                    $sql .= "`" . $campo[0] . "`='" . $_POST[$campo[0]] . "',";
                }
            }
            $sql = substr($sql, 0, -1) . " WHERE id = " . $_GET["id"];
            $result = $objCon->only($sql);
            if ($result == FALSE) {
                echo json_encode(["error", "No se actualizo..."]);
            } else {
                echo json_encode(["sucess", "Se actualizo correctamente.."]);
            }
            // borro los roles anteriores.
            $sqlDel = "Delete from empleado_rol where empleado_id = ".  $_GET["id"];
            $resulDel = $objCon->only($sqlDel);
            // creo los nuevos.
            $sqlRol = "INSERT into empleado_rol values ";
            foreach ($_POST["roles"] as $rol) {
                $sqlRol .= "(" . $_GET["id"] . "," . $rol . "),";
            }
            $sqlRol =  substr($sqlRol, 0, -1) . ";";
            $resulRol = $objCon->only($sqlRol);
            if ($resulRol == false) {
                echo json_encode(["error", "No se registro..."]);
            } else {
                echo json_encode(["succes", "Se registro Correctamente"]);
            }
        }
        break;
    case "DELETE":
        $objCon = new Conectar();
        if (!isset($_GET["id"])) {
            echo json_encode(["error", "Se necesita un id para eliminar..."]);
            die;
        }
        //Se eliminan los roles de ese empleado
        $sqlrol = "DELETE From empleado_rol where empleado_id = " . $_GET["id"] . "; ";
        $resultrol = $objCon->only($sqlrol);
        // Se arma el query y se elimina el registro del empleado.
        $sql = "DELETE From empleado where id = " . $_GET["id"] . ";";
        $result = $objCon->only($sql);
        if ($result == FALSE) {
            echo json_encode(["error", "No se elimino..."]);
        } else {
            echo json_encode(["sucess", "Se elimino correctamente.."]);
        }
        break;
}
