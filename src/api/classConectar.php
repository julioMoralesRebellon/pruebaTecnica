<?php
    /*
     *  Creado por Julio César Morales R.
     *  Base para prueba de php.
     * */

	class Conectar 
	{
		private $server;
		private $user;
		private $pass;
	    private $basedatos;
		private $conexion;
		 
	 	function __construct()
		{
			$this->setConect();
			$this->connect();
		
		}
		/**--------------------------Operadores de la base de datos.---------------------------**/
		private function setConect()
		{
			 $this->server="db";
			 $this->user="root";
			 $this->pass="test";
			 $this->basedatos="prueba_tecnica_dev";
			 $this->port="3306";	
		}
					
		private  function connect()
		{
			$this->conexion=@mysqli_connect($this->server, $this->user, $this->pass, $this->basedatos);

			if (!$this->conexion)
			{
					error_log(0,"error al conectar");
					exit ("intentelo mas tarde");
			}
		
		}
        function exe($query)
        {
            $ejecucion=@mysqli_query($this->conexion,$query);
            mysqli_query($this->conexion,"SET NAMES 'latin1'");
            $resultado=array();

            while($rows=@mysqli_fetch_row($ejecucion))
            {
                array_push($resultado,$rows);
            }
            return ["data" => $resultado];
        }
        function num($query)
        {
            $ejecucion=@mysqli_query($this->conexion,$query);
            mysqli_query($this->conexion,"SET NAMES 'latin1'");

            $num=@mysqli_num_rows($ejecucion);
            return $num;
        }
		function onlyID($query)
        {
             $ejecucion=@mysqli_query($this->conexion,$query);
			 if($ejecucion === true){
				return $this->conexion->insert_id;
			 } else {
				return false;
			 }
			 
            
        }
        function only($query)
        {
            return $ejecucion=@mysqli_query($this->conexion,$query);
            
        }

	}
?>