<!DOCTYPE html>
<html>

<head>
    <title>Prueba_dev</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.3/font/bootstrap-icons.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.12.1/css/jquery.dataTables.min.css">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/notify/0.4.2/notify.min.js" integrity="sha512-efUTj3HdSPwWJ9gjfGR71X9cvsrthIA78/Fvd/IN+fttQVy7XWkOAXb295j8B3cmm/kFKVxjiNYzKw9IQJHIuQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.3/dist/jquery.validate.min.js"></script>
    <script src="js/index.js"></script>
    <style>
        label.error {
            color: red;
            font-family: verdana, Helvetica;
        }
    </style>
</head>

<body>
    <div class="container-fluid">
        <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#newEmpl" id="newEmpModal">
            Crear empleado
        </button>
        <table class="table table-striped" id="empleados">
            <thead>
                <tr>
                    <th>id</th>
                    <th><i class="bi bi-person-fill"></i> Nombre</th>
                    <th><i class="bi bi-at"></i> Email</th>
                    <th><i class="bi bi-gender-ambiguous"></i> Sexo</th>
                    <th><i class="bi bi-briefcase-fill"></i> Área</th>
                    <th><i class="bi bi-envelope-fill"></i> Boletín</th>
                    <th>Acciones</th>
                </tr>
            </thead>
            <tbody>

            </tbody>
        </table>
    </div>

    <div class="modal" id="newEmpl">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Crear Empleado.</h4>
                    <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
                </div>
                <div class="modal-body">
                    <form id="formEmpleado">
                        <input type="hidden" name="id" id="id">
                        <div class="row">
                            <div class="col-sm-3">
                                <label for="name" class="form-label">Nombre:</label>
                            </div>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="name" name="name" placeholder="Nombre" required>
                            </div>
                        </div>
                        <div class="row mt-2">
                            <div class="col-sm-3">
                                <label for="name" class="form-label">Correo electrónico:</label>
                            </div>
                            <div class="col-sm-9">
                                <input type="email" class="form-control" id="email" name="email" placeholder="Correo electrónico" required>
                            </div>
                        </div>
                        <div class="row mt-2">
                            <div class="col-sm-3">
                                <label for="sexo" class="form-label">Sexo:</label>
                            </div>
                            <div class="col-sm-9">
                                <div class="form-check">
                                    <input type="radio" class="form-check-input" id="M" name="sexo" value="M">
                                    <label class="form-check-label" for="M">Masculino</label>
                                </div>
                                <div class="form-check">
                                    <input type="radio" class="form-check-input" id="F" name="sexo" value="F">
                                    <label class="form-check-label" for="F">Femenino</label>
                                </div>
                            </div>
                        </div>
                        <div class="row mt-2">
                            <div class="col-sm-3">
                                <label for="area" class="form-label">Área:</label>
                            </div>
                            <div class="col-sm-9">
                                <select class="form-select" id="area" name="area"></select>
                            </div>
                        </div>
                        <div class="row mt-2">
                            <div class="col-sm-3">
                                <label for="descripcion" class="form-label">Descripción:</label>
                            </div>
                            <div class="col-sm-9">
                                <textarea class="form-control" rows="5" id="descripcion" name="descripcion"></textarea>
                            </div>
                        </div>
                        <div class="row mt-2">
                            <div class="col-sm-3"></div>
                            <div class="col-sm-9">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" id="boletin1" name="boletin" value="1">
                                    <label for="boletin1" class="form-check-label">Deseo recibir boletín informativo</label>
                                </div>
                            </div>
                        </div>
                        <div class="row mt-2">
                            <div class="col-sm-3">
                                <label>Roles</label>
                            </div>
                            <div class="col-sm-9" id="roles">
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <div class="spinner-border text-muted" id="loading" style="display: none;"></div>
                    <button type="button" class="btn btn-primary" id="saveEmpleado">Guardar</button>
                    <button type="button" class="btn btn-primary" id="actEmpleado" style="display: none;">Actualizar</button>
                    <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Cerrar</button>
                </div>

            </div>
        </div>
    </div>


</body>

</html>