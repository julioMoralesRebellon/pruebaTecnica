$(document).ready(function () {
    // Data table de jquery para mostrar los datos de la tabla, esta ya contiene el ajax que llama el api.
    var table = $('#empleados').DataTable({
        ajax: "api/empleados.php",
        language: {
            "url": "js/leng.json"
        },
        processing: true,
        responsive: true,
        columnDefs: [{
            targets: -1,
            data: null,
            defaultContent: "<a class='edit' title='Editar Empleado'><i class='bi bi-pencil-fill'></i>&nbsp</a><a class='delete' title='Eliminar Empleado'><i class='bi bi-archive-fill'></i></a>"
        }]
    });
    // metodo para ejecutar la actualizacion de un empleado
    $('#empleados tbody').on('click', '.edit', function () {
        clearForm();
        var data = table.row($(this).parents('tr')).data();
        $("#id").val(data[0]);
        $("#name").val(data[1]);
        $("#email").val(data[2]);
        $('#' + data[3]).prop('checked', true);
        $("#area").val(data[7]);
        $('#boletin1').prop('checked', data[6]);
        $("#descripcion").val(data[8]);
        $.ajax({
            type: "GET",
            dataType: 'json',
            url: "api/empleado_roles.php?id=" + data[0],
            beforeSend: function () { },
            success: function (data) {
                data.data.forEach(roles => {
                    roles.forEach(rol => {
                        $("#rol" + rol).prop('checked', true);
                    })
                });

            },
            error: function (data) {
            }
        });
        $("#saveEmpleado").hide();
        $("#actEmpleado").show();
        $("#newEmpl").modal('show');
    });
    // Metodo para eliminar un empleado, se llama el api y se espera los mensajes de retorno para recargar la tabla
    $('#empleados tbody').on('click', '.delete', function () {
        var data = table.row($(this).parents('tr')).data();
        var promp = confirm("estas seguro de eliminar el empleado " + data[1] + '?');
        if (promp) {
            $.ajax({
                type: "delete",
                url: "api/empleados.php?id=" + data[0],
                beforeSend: function () {
                },
                success: function (data) {
                    table.ajax.reload();
                    $.notify("Se elimino correctamente.", "success");
                },
                error: function (data) {
                    $.notify("No se puede borrar...", "warn");
                }
            });
        }
    });
    // Metodos custom para los validadores de jquery, se coloca los mensajes en español.
    jQuery.validator.addMethod("notEqual", function (value, element, param) {
        return this.optional(element) || value != param;
    }, "Selecciona un area");
    $("#formEmpleado").validate(
        {
            rules:
            {
                name: { required: true },
                email: { required: true, email: true },
                area: { required: true, notEqual: '00' },
                descripcion: { required: true },
                sexo: { required: true },
                roles: {
                    required: true,
                    minlength: 1
                },
            },
            messages:
            {
                name:
                {
                    required: "Ingresa un nombre"
                },
                email:
                {
                    required: "Ingresa un correo electrónico",
                    email: "Ingresa un correo valido"
                },
                area:
                {
                    required: "Selecciona un area"
                },
                descripcion:
                {
                    required: "ingresa una descripcion"
                },
                sexo:
                {
                    required: "Selecciona un sexo"
                },
                roles:
                {
                    required: "Selecciona un rol"
                }
            },
            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    error.appendTo(element.parents('.col-sm-9'));
                }
                else {
                    error.insertAfter(element);
                }
            }
        });
    $("#newEmpModal").click(function (params) {
        $("#saveEmpleado").show();
        $("#actEmpleado").hide();
    });
    // Metodo de clic para el boton de guardar
    $("#saveEmpleado").click(function () {
        $("#formEmpleado").submit();
        if ($("#formEmpleado").valid()) {
            // se definen todos los atributos del empleado desde el formulario para ser enviados al api.
            var roles = [];
            $('#roles input:checked').each(function () {
                roles.push($(this).attr('value'));
            });
            var data = {
                "nombre": $("#name").val(),
                "email": $("#email").val(),
                "sexo": $('input[name=sexo]:checked').val(),
                "area_id": $("#area").val(),
                "boletin": $('#boletin1').is(':checked') ? 1 : 0,
                "descripcion": $("#descripcion").val(),
                "roles": roles

            };
            // Medoto Ajax que genera la peticion POST para guardar los datos.
            $.ajax({
                type: "POST",
                url: "api/empleados.php",
                data: data,
                beforeSend: function () {
                    $("#loading").show();
                },
                success: function (data) {
                    table.ajax.reload();
                    $.notify("Se guardo correctamente.", "success");
                    $("#loading").hide();
                    $("#newEmpl").modal('hide');
                    clearForm();
                },
                error: function (data) {
                    table.ajax.reload();
                    $.notify("no se guardo...", "error");
                    $("#loading").hide();
                }
            });
        }
    });
    $("#actEmpleado").click(function () {
        $("#formEmpleado").submit();
        if ($("#formEmpleado").valid()) {
            // se definen todos los atributos del empleado desde el formulario para ser enviados al api.
            var roles = [];
            $('#roles input:checked').each(function () {
                roles.push($(this).attr('value'));
            });
            var data = {
                "nombre": $("#name").val(),
                "email": $("#email").val(),
                "sexo": $('input[name=sexo]:checked').val(),
                "area_id": $("#area").val(),
                "boletin": $('#boletin1').is(':checked') ? 1 : 0,
                "descripcion": $("#descripcion").val(),
                "roles": roles

            };
            // Medoto Ajax que genera la peticion POST para guardar los datos.
            $.ajax({
                type: "POST",
                url: "api/empleados.php?id="+$("#id").val(),
                data: data,
                beforeSend: function () {
                    $("#loading").show();
                },
                success: function (data) {
                    table.ajax.reload();
                    $.notify("Se actualizo correctamente.", "success");
                    $("#loading").hide();
                    $("#newEmpl").modal('hide');
                    clearForm();
                },
                error: function (data) {
                    table.ajax.reload();
                    $.notify("no se actualizo...", "error");
                    $("#loading").hide();
                }
            });
        }
    });
    // Evento para prevenir el submit normal de los formularios (el proceso es asincrono) 
    $("#formEmpleado").submit(function (event) {
        event.preventDefault();
    });
    // GET basico a el api para el mantenimiento de las areas.
    $.ajax({
        type: "get",
        url: "api/areas.php",
        dataType: 'json',
        beforeSend: function () {
        },
        success: function (data) {
            var content = '<option value="00">Selecciona</option>';
            data.data.forEach(area => {
                content += '<option value="' + area[0] + '">' + area[1] + '</option>';
            });
            $("#area").html(content);
        },
        error: function (data) {

        }
    });
    // GET basico a el api para el mantenimiento de los roles.
    $.ajax({
        type: "get",
        url: "api/roles.php",
        dataType: 'json',
        beforeSend: function () {
        },
        success: function (data) {
            var content = "";
            data.data.forEach(rol => {
                content += '<div class="form-check">';
                content += ' <label for="rol' + rol[0] + '" class="form-check-label">' + rol[1] + '</label>';
                content += '<input class="form-check-input" type="checkbox" id="rol' + rol[0] + '" name="roles" value="' + rol[0] + '">';
                content += '</div>';

            });
            $("#roles").html(content);
        },
        error: function (data) {

        }
    });
});
// Metodo para limpiar el formulario.
function clearForm(params) {
    // recorro los roles para dejarlos en false
    $('#roles input:checked').each(function () {
        $(this).prop('checked', false);
    });
    // se limpia el resto de los campos
    $("#name").val("");
    $("#email").val("");
    $('input[name=sexo]:checked').prop('checked', false);
    $("#area").val("00");
    $('#boletin1').prop('checked', false);
    $("#descripcion").val("");

}
