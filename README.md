# PruebaTecnica

Este repo se crea para alojar la prueba técnica

## Getting started

Esta prueba tecnica fue desarollada en un contenedor docker, solo se debe hacer el pull a la imagen y se accedera a la misma.

Se debera utilizar este comando para desplegar la infraestructura.
```
docker-compose up -d
```
para correr el cliente de mysql 

- `docker-compose exec db mysql -u root -p` 

En caso de no contar con docker, solo se debe mover la carpeta /src a un directorio del cualquier servidor apache y correr la migracion en el directorio /dump